const mongoose = require('mongoose');
const config = require('./config');

const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const artists = await Artist.create(
        {name: 'Queen'},
        {name: 'The Prodigy'}
    );

    const albums = await Album.create(
        {
            title: 'Sheer Heart Attack',
            artist: artists[0]._id,
            release_date: '1974'
        },
        {
            title: 'Worlds On Fire',
            artist: artists[1]._id,
            release_date: '2011'
        }
    );

    await Track.create(
        {
            title: 'Brighton Rock',
            album: albums[0]._id,
            duration: '5:10'
        },
        {
            title: 'Killer Queen',
            album: albums[0]._id,
            duration: '2:59'
        },
        {
            title: 'Breathe',
            album: albums[1]._id,
            duration: '5:02'
        },
        {
            title: 'Omen',
            album: albums[1]._id,
            duration: '3:48'
        }
    );

    await connection.close();
};

run().catch(error => {
    console.error('Something went wrong', error);
});
