const express = require('express');

const Track = require('../models/Track');

const router = express.Router();

router.get('/', (req, res) => {
    Track.find()
        .then(traks => res.send(traks))
        .catch(() => res.sendStatus(404));
});

module.exports = router;